## TTGO T-Beam Sensor


LoraMessage erzeugt einen Speicherfehler wenn als erstes die GPS Koordinaten hinzugefügt werden. Daher wird zuerst ein DummyByte gesendet. Man könnte auch die Uhrzeit oder etwas anderes schicken, nur nicht GPS Koordinaten.

Current version: 1.1.1

Uploads sensor data from the TTGO T-Beam to [The Things Network](https://www.thethingsnetwork.org) (TTN).

#### Based on the code from [ kizniche/ttgo-tbeam-ttn-tracker](https://github.com/kizniche/ttgo-tbeam-ttn-tracker)

This is a LoRaWAN node based on the [TTGO T-Beam](https://github.com/LilyGO/TTGO-T-Beam) development platform using the SSD1306 I2C OLED display.
It uses a RFM95 by HopeRF and the MCCI LoRaWAN LMIC stack. This sample code is configured to connect to The Things Network using the US 915 MHz frequency by default, but can be changed to EU 868 MHz.

NOTE: There are now 2 versions of the TTGO T-BEAM, the first version (Rev0) and a newer version (Rev1). The GPS module on Rev1 is connected to different pins than Rev0. This code has been successfully tested on REV0, and is in the process of being tested on REV1. See the end of this README for photos of eah board.

### Setup

1. Follow the directions at [espressif/arduino-esp32](https://github.com/espressif/arduino-esp32) to install the board to the Arduino IDE and use board 'T-Beam'.

2. Install the Arduino IDE libraries:

   * [mcci-catena/arduino-lmic](https://github.com/mcci-catena/arduino-lmic) (for Rev0 and Rev1)
   * [mikalhart/TinyGPSPlus](https://github.com/mikalhart/TinyGPSPlus) (for Rev0 and Rev1)
   * [ThingPulse/esp8266-oled-ssd1306](https://github.com/ThingPulse/esp8266-oled-ssd1306) (for Rev0 and Rev1)
   * [lewisxhe/AXP202X_Library](https://github.com/lewisxhe/AXP202X_Library) (for Rev1 only)
   * [adafruit/BME280_library](https://github.com/adafruit/Adafruit_BME280_Library) (use the library manager and install the dependencies as well)
   * [adafruit Sensor](https://github.com/adafruit/Adafruit_Sensor)
   * [adafruit_ADXL343](https://github.com/adafruit/Adafruit_ADXL345)(dependency? not needed for the sensor)
   * [ dok-net/esp_sds011](https://github.com/dok-net/esp_sds011)(use the library manager and install the dependency ESP SoftwareSerial)
   * [ESP SoftwareSerial](https://github.com/plerup/espsoftwareserial/)
   * [Adafruid ADS1X15](https://github.com/adafruit/Adafruit_ADS1X15)

3. Copy the contents of the project file ```main/lmic_project_config.h``` to the library file ```arduino-lmic/project_config/lmic_project_config.h``` and uncomment the proper frequency for your region.

4. Edit this project file ```main/configuration.h``` and select your correct board revision, either T_BEAM_V07 or T_BEAM_V10 (see [T-BEAM Board Versions](#t-beam-board-versions) to determine which board revision you have).

5. Edit this project file ```main/credentials.h``` to use either ```USE_ABP``` or ```USE_OTAA``` and add the Keys/EUIs for your Application's Device from The Things Network.


6. Open this project file ```main/main.ino``` with the Arduino IDE and upload it to your TTGO T-Beam.

7. Turn on the device and once a GPS lock is acquired, the device will start sending data to TTN.


### T-BEAM Board Versions

#### Rev0

![TTGO T-Beam 01](img/TTGO-TBeam-01.jpg)

![TTGO T-Beam 02](img/TTGO-TBeam-02.jpg)

![TTGO T-Beam 03](img/TTGO-TBeam-03.jpg)

#### Rev1

![T-BEAM-Rev1-01](img/T-BEAM-Rev1-01.jpg)

![T-BEAM-Rev1-02](img/T-BEAM-Rev1-02.jpg)

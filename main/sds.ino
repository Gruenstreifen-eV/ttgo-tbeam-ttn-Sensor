/*

  SDS011 module

  Copyright (C) 2020 by Jörg stadler

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <SoftwareSerial.h>
#include <Sds011.h>

SoftwareSerial serialSDS;
Sds011Async< SoftwareSerial > sds011(serialSDS);

int pm25_table[pm_tablesize];
int pm10_table[pm_tablesize];
bool is_SDS_running = true;
int pm25;
int pm10;

float get_pm25() {
    return pm25;
}

float get_pm10() {
    return pm10;
}

void start_SDS() {
  Serial.println("Start wakeup SDS011");

  if (sds011.set_sleep(false)) { is_SDS_running = true; }

  Serial.println("End wakeup SDS011");
}

void stop_SDS() {
  Serial.println("Start sleep SDS011");

  if (sds011.set_sleep(true)) { is_SDS_running = false; }

  Serial.println("End sleep SDS011");
}


void sds_setup() {
    serialSDS.begin(9600, SWSERIAL_8N1, SDS_PIN_RX, SDS_PIN_TX, false, 192);
    Sds011::Report_mode report_mode;
    if (!sds011.get_data_reporting_mode(report_mode)) {
        Serial.println("Sds011::get_data_reporting_mode() failed");
    }
    if (Sds011::REPORT_ACTIVE != report_mode) {
        Serial.println("Turning on Sds011::REPORT_ACTIVE reporting mode");
        if (!sds011.set_data_reporting_mode(Sds011::REPORT_ACTIVE)) {
            Serial.println("Sds011::set_data_reporting_mode(Sds011::REPORT_ACTIVE) failed");
        }
    }

}

static void sds_loop() {
	  // Per manufacturer specification, place the sensor in standby to prolong service life.
    // At an user-determined interval, run the sensor for duty_s sec.
    // Quick response time is given as 10s by the manufacturer, thus omit the measurements
    // obtained during the first 10s of each run.
    static uint32_t last_sds = 0;
    if (0 == last_sds || millis() - last_sds > (SEND_INTERVAL - duty_s * 1000)) {
      start_SDS();
      Serial.print("started SDS011 (is running = ");
      Serial.print(is_SDS_running);
      Serial.println(")");
      
      sds011.on_query_data_auto_completed([](int n) {
        Serial.println("Begin Handling SDS011 query data");
        Serial.print("n = "); Serial.println(n);
        if (sds011.filter_data(n, pm25_table, pm10_table, pm25, pm10) &&
            !isnan(pm10) && !isnan(pm25)) {
            Serial.print("PM10: ");
            Serial.println(float(pm10) / 10);
            Serial.print("PM2.5: ");
            Serial.println(float(pm25) / 10);
            last_sds = millis();
        }
        Serial.println("End Handling SDS011 query data");
        });

      if (!sds011.query_data_auto_async(pm_tablesize, pm25_table, pm10_table)) {
          Serial.println("measurement capture start failed");
      }
  
      uint32_t deadline = millis() + duty_s * 1000;
      while (static_cast<int32_t>(deadline - millis()) > 0) {
          delay(1000);
          Serial.println(static_cast<int32_t>(deadline - millis()) / 1000);
          sds011.perform_work();
      }
    }
    else {
    if (is_SDS_running){
      stop_SDS();
      Serial.print("stopped SDS011 (is running = ");
      Serial.print(is_SDS_running);
      Serial.println(")");
    }
    }  

}

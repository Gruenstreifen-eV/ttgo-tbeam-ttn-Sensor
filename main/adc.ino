/*

  SDS011 module

  Copyright (C) 2020 by Jörg stadler

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <Adafruit_ADS1015.h>
Adafruit_ADS1115 ads1115;
float adc0, adc1, adc2, adc3, uvIndex; //V


float get_adc0() {
    return adc0;
}

float get_adc1() {
    return adc1;
}

float get_adc2() {
    return adc2;
}

float get_adc3() {
    return adc3;
}

float get_uvIndex() {
    return uvIndex;
}


void adc_setup() {
    ads1115.begin();
}

static void adc_loop() {
    ads1115.begin();
    float scale = 6.114/32767;
    uint16_t uvIndexValue [12] = { 50, 227, 318, 408, 503, 606, 696, 795, 881, 976, 1079, 1170};

    adc0 = ads1115.readADC_SingleEnded(0) * scale; 
    adc1 = ads1115.readADC_SingleEnded(1) * scale;
    adc2 = ads1115.readADC_SingleEnded(2) * scale;
    adc3 = ads1115.readADC_SingleEnded(3) * scale;

    float uv = adc0 * 1000.0; //mV
	int i;
	for (i = 0; i < 12; i++) {
	    if (uv <= uvIndexValue[i]) {
	        uvIndex = i;
	        break;
	    }
	}
	//calculate 1 decimal if possible
	if (i>0) {
	    float vRange=uvIndexValue[i]-uvIndexValue[i-1];
	    float vCalc=uv -uvIndexValue[i-1];
	    uvIndex+=(1.0/vRange)*vCalc-1.0;
	}

}
